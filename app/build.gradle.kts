plugins {
    id ("com.android.application")
    id ("kotlin-android")
    id ("kotlin-android-extensions")
    id ("androidx.navigation.safeargs.kotlin")
    id ("kotlin-kapt")
    id ("dagger.hilt.android.plugin")
}

android {
    compileSdkVersion (30)
    buildToolsVersion ("30.0.2")

    defaultConfig {
        applicationId = "com.example.ejemplouala"
        minSdkVersion (21)
        targetSdkVersion (30)
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables.useSupportLibrary = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles (getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    buildFeatures {
        dataBinding = true
    }

    androidExtensions {
        isExperimental = true
    }

    lintOptions {
        isAbortOnError = false
    }

    /*sourceSets {
        androidTest.java.srcDirs += "src/test-common/java"
        test.java.srcDirs += "src/test-common/java"
        test.assets.srcDirs += files("$projectDir/schemas".toString())
    }*/

    testOptions {
        unitTests {
            isIncludeAndroidResources = true
            isReturnDefaultValues = true
        }
    }

    packagingOptions {
        exclude ("META-INF/DEPENDENCIES")
        exclude ("META-INF/LICENSE")
        exclude ("META-INF/LICENSE.txt")
        exclude ("META-INF/license.txt")
        exclude ("META-INF/NOTICE")
        exclude ("META-INF/NOTICE.txt")
        exclude ("META-INF/notice.txt")
        exclude ("META-INF/ASL2.0")
        exclude ("META-INF/*.kotlin_module")
    }
}

kapt {
    correctErrorTypes = true
}

dependencies {

    implementation ("org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}")
    implementation ("androidx.core:core-ktx:1.3.2")
    implementation ("androidx.appcompat:appcompat:1.2.0")
    implementation ("com.google.android.material:material:1.2.1")
    implementation ("androidx.constraintlayout:constraintlayout:2.0.2")
    implementation ("androidx.legacy:legacy-support-v4:1.0.0")

    testImplementation ("junit:junit:4.13.1")
    androidTestImplementation ("androidx.test.ext:junit:1.1.2")
    androidTestImplementation ("androidx.test.espresso:espresso-core:3.3.0")

    /*                  ARQUITECTURA DE COMPONENTES                 */
    // Fragment KTX
    implementation ("androidx.fragment:fragment-ktx:${Versions.fragment}")
    // LifeCycle, ViewModel and LiveData
    implementation ("androidx.lifecycle:lifecycle-extensions:${Versions.lifeCycle}")
    implementation ("androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifeCycle}")
    implementation ("androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifeCycle}")
    implementation ("androidx.lifecycle:lifecycle-viewmodel-savedstate:${Versions.lifeCycle}")
    // Room
    implementation ("androidx.room:room-runtime:${Versions.room}")
    implementation ("androidx.room:room-ktx:${Versions.room}")
    kapt ("androidx.room:room-compiler:${Versions.room}")
    // Core
    testImplementation ("androidx.arch.core:core-testing:${Versions.architectureComponent}")
    // Navigation
    implementation ("androidx.navigation:navigation-fragment-ktx:${Versions.navigation}")
    implementation ("androidx.navigation:navigation-ui-ktx:${Versions.navigation}")

    /*                  COROUTINES                 */
    implementation ("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.coroutines}")
    implementation ("org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutines}")
    testImplementation ("org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutines}")

    /*                  HILT                 */
    implementation ("com.google.dagger:hilt-android:${Versions.hilt}")
    implementation ("androidx.hilt:hilt-lifecycle-viewmodel:${Versions.hilt_viewmodel}")
    kapt ("com.google.dagger:hilt-android-compiler:${Versions.hilt}")
    kapt ("androidx.hilt:hilt-compiler:${Versions.hilt_viewmodel}")
    implementation("androidx.activity:activity-ktx:${Versions.activity_ktx}")

    /*                  RETROFIT2                 */
    implementation ("com.squareup.retrofit2:retrofit:${Versions.retrofit}")
    implementation("com.squareup.retrofit2:converter-moshi:${Versions.retrofit}")

    /*                  OKHTTP3                 */
    implementation ("com.squareup.okhttp3:okhttp:${Versions.okHttp}")
    implementation ("com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}")
    implementation ("com.squareup.okhttp3:okhttp-urlconnection:${Versions.okHttp}")
    testImplementation("com.squareup.okhttp3:mockwebserver:${Versions.okHttp}")

    /*                  TIMBER                 */
    implementation ("com.jakewharton.timber:timber:${Versions.timber}")

    /*                  MOSHI                 */
    implementation ("com.squareup.moshi:moshi-kotlin:${Versions.moshi}")
    kapt ("com.squareup.moshi:moshi-kotlin-codegen:${Versions.moshi}")

    ////////////////////////////////////////////////////////////////////////////////////////////////

    // unit test
    /*testImplementation "junit:junit:$versions.junitVersion"
    testImplementation "androidx.test:core:$versions.androidxTest"
    testImplementation "com.nhaarman.mockitokotlin2:mockito-kotlin:$versions.mockitoKotlinVersion"
    testImplementation "org.mockito:mockito-inline:$versions.mockitoInlineVersion"
    testImplementation "app.cash.turbine:turbine:$versions.turbineVersion"
    testImplementation "org.robolectric:robolectric:$versions.robolectricVersion"
    androidTestImplementation "com.google.truth:truth:$versions.truthVersion"
    androidTestImplementation "androidx.test.ext:junit:$versions.androidxTestJunit"
    androidTestImplementation "com.android.support.test:runner:$versions.androidTestRunner"
    androidTestImplementation "androidx.test.espresso:espresso-core:$versions.espressoVersion"*/

    /*
        testImplementation 'junit:junit:4.12'
    implementation 'androidx.test:core:1.2.0'
    implementation 'androidx.test.ext:junit:1.1.1'
    implementation 'androidx.test:runner:1.2.0'
    implementation 'androidx.test:rules:1.2.0'
    implementation 'androidx.test.ext:junit:1.1.1'
    androidTestImplementation 'org.mockito:mockito-core:3.1.0'
    debugImplementation 'androidx.fragment:fragment-testing:1.2.2'
    androidTestImplementation "androidx.arch.core:core-testing:2.1.0"
    androidTestImplementation 'androidx.test:core:1.2.0'
    androidTestImplementation 'androidx.test.ext:junit:1.1.1'
    androidTestImplementation 'androidx.test:runner:1.2.0'
    androidTestImplementation 'androidx.test:rules:1.2.0'
    debugImplementation 'androidx.fragment:fragment-testing:1.2.2'
    androidTestImplementation 'org.mockito:mockito-core:3.1.0'
    testImplementation 'org.mockito:mockito-core:3.1.0'
    testImplementation 'org.mockito:mockito-inline:3.1.0'
    testImplementation 'androidx.arch.core:core-testing:2.1.0'
    testImplementation 'org.jetbrains.kotlinx:kotlinx-coroutines-test:1.3.3'
    implementation 'junit:junit:4.12'
    testImplementation 'org.robolectric:robolectric:4.3'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'
     */
}