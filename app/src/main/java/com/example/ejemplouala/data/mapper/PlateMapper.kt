package com.example.ejemplouala.data.mapper

import com.example.ejemplouala.data.dto.MealsListResponse
import com.example.ejemplouala.domain.model.Plate
import okhttp3.internal.toImmutableList

class PlateMapper: Mapper<List<MealsListResponse>, List<Plate>> {
    override suspend fun map(input: List<MealsListResponse>): List<Plate> {
        val plateList: MutableList<Plate> = mutableListOf()
        for (index in input.indices) {
            plateList.add(
                Plate(input[index].idMeal, input[index].strMealThumb,
                    input[index].strMeal, input[index].strCategory,
                    input[index].strInstructions)
            )
        }
        return plateList.toImmutableList()
    }
}