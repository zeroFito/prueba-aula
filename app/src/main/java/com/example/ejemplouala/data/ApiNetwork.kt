package com.example.ejemplouala.data

import androidx.lifecycle.LiveData
import com.example.ejemplouala.domain.model.Plate
import com.example.ejemplouala.presentation.framework.library.retrofit.Resource

interface ApiNetwork {
    fun getListPlate(plate: String): LiveData<Resource<List<Plate>>>
}