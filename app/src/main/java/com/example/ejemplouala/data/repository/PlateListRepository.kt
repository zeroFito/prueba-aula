package com.example.ejemplouala.data.repository

import com.example.ejemplouala.data.ApiNetwork
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PlateListRepository @Inject constructor(private val apiNetwork: ApiNetwork) {
    fun getPlates(plate: String) = apiNetwork.getListPlate(plate)
}