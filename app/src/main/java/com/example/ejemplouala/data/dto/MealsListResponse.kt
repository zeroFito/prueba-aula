package com.example.ejemplouala.data.dto

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.ejemplouala.presentation.utils.Constants.Persistence.TABLE_NAME
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
@Entity(tableName = TABLE_NAME)
data class MealsListResponse (
    @field:Json(name = "idMeal") @PrimaryKey val idMeal: String,
    @field:Json(name = "strMealThumb") val strMealThumb: String,
    @field:Json(name = "strMeal") val strMeal: String,
    @field:Json(name = "strCategory") val strCategory: String,
    @field:Json(name = "strInstructions") val strInstructions: String
)