package com.example.ejemplouala.data.mapper

interface Mapper<I, O> {
    suspend fun map(input: I): O
}