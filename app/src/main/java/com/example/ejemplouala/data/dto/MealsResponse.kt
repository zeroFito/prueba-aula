package com.example.ejemplouala.data.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MealsResponse (
    @field:Json(name = "meals") val meals: List<MealsListResponse>
)