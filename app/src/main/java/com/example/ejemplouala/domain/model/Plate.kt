package com.example.ejemplouala.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Plate (val id: String,
                  val photo: String,
                  val name: String,
                  val category: String,
                  val instructions: String): Parcelable