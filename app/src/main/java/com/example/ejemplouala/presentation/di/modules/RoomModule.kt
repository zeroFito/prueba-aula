package com.example.ejemplouala.presentation.di.modules

import android.app.Application
import androidx.room.Room
import com.example.ejemplouala.presentation.framework.library.room.AppDataBase
import com.example.ejemplouala.presentation.framework.library.room.dao.MealDao
import com.example.ejemplouala.presentation.utils.Constants.Persistence.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class RoomModule {

    @Singleton
    @Provides
    fun provideDb(application: Application): AppDataBase =
            Room.databaseBuilder(application.applicationContext, AppDataBase::class.java, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build()

    @Singleton
    @Provides
    fun provideMealDao(appDataBase: AppDataBase): MealDao = appDataBase.mealDao()
}