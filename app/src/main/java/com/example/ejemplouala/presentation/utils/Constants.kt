package com.example.ejemplouala.presentation.utils

class Constants {
    object Util {
        const val VIEW_MODEL_NOT_FOUND = "ViewModel Not Found"
    }

    object Apis {
        const val URL = "https://www.themealdb.com/api/json/v1/1/"

        const val ENDPOINT_SEARCH = "search.php"
        const val ENDPOINT_LOOKUP = "lookup.php"
        const val ENDPOINT_RANDOM = "random.php"
    }

    object Persistence {
        const val DATABASE_VERSION = 1
        const val TABLE_NAME = "Meal"
        const val DATABASE_NAME = "RoomIMEI.db"
    }
}