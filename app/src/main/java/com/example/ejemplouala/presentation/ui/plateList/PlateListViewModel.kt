package com.example.ejemplouala.presentation.ui.plateList

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.example.ejemplouala.data.repository.PlateListRepository

class PlateListViewModel @ViewModelInject
constructor(private val plateListRepository: PlateListRepository) : ViewModel() {
    private val requestGetPlateListLiveData = plateListRepository.getPlates("")

    fun getPlateLiveData() = requestGetPlateListLiveData
}