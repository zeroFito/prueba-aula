package com.example.ejemplouala.presentation.framework.implementation.strategy

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import androidx.lifecycle.map
import com.example.ejemplouala.presentation.framework.library.retrofit.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking

fun <T, A, S> resultLiveData(
    databaseQuery: () -> LiveData<S>,
    networkCall: suspend () -> Resource<A>,
    saveCallResult: suspend (A) -> Unit,
    mapper: suspend (S) -> T
): LiveData<Resource<T>> =
    liveData(Dispatchers.IO) {
        emit(Resource.loading())
        val source = databaseQuery.invoke().map {
            Resource.success( runBlocking { mapper(it) })
        }
        emitSource(source)

        val responseStatus = networkCall.invoke()
        if (responseStatus.status == Resource.Status.SUCCESS) {
            saveCallResult(responseStatus.data!!)
        } else if (responseStatus.status == Resource.Status.ERROR) {
            emit(Resource.error(responseStatus.message!!))
            emitSource(source)
        }
    }