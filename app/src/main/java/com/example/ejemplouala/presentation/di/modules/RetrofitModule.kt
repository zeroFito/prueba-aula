package com.example.ejemplouala.presentation.di.modules

import android.app.Application
import com.example.ejemplouala.BuildConfig
import com.example.ejemplouala.data.ApiNetwork
import com.example.ejemplouala.presentation.framework.library.retrofit.IRetrofitService
import com.example.ejemplouala.presentation.framework.library.retrofit.RemoteDataSource
import com.example.ejemplouala.presentation.framework.implementation.ImplementationCallRetrofit
import com.example.ejemplouala.presentation.framework.library.room.dao.MealDao
import com.example.ejemplouala.presentation.utils.Constants.Apis.URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Level.NONE
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class RetrofitModule {
    @Provides
    @Singleton
    fun provideCache(application: Application): Cache {
        val cacheSize = 25 * 1024 * 1024 // 25 MiB
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Singleton
    fun provideInterceptor(): HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        level = if (BuildConfig.DEBUG) BODY else NONE
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor, cache: Cache) = OkHttpClient().newBuilder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
        .retryOnConnectionFailure(false)
        .addInterceptor(httpLoggingInterceptor)
        .cache(cache)
        .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(URL)
        .client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideApiWebservice(restAdapter: Retrofit): IRetrofitService = restAdapter.create(
        IRetrofitService::class.java)

    @Provides
    @Singleton
    fun provideGetCampusRemoteDataSource(iRetrofitService: IRetrofitService): RemoteDataSource
            = RemoteDataSource(iRetrofitService)

    @Provides
    @Singleton
    fun provideRemoteDataSource(remoteDataSource: RemoteDataSource, mealDao: MealDao): ApiNetwork
            = ImplementationCallRetrofit(remoteDataSource, mealDao)
}