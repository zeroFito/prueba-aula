package com.example.ejemplouala.presentation.ui.plateList.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.ejemplouala.R
import com.example.ejemplouala.domain.model.Plate
import com.example.ejemplouala.presentation.utils.inflate
import kotlinx.android.synthetic.main.row_plate.view.*
import java.util.*
import kotlin.collections.ArrayList

class CustomAdapterPlateList(private val listPlate: List<Plate>,
                             private val listener: (Int) -> Unit) :
        RecyclerView.Adapter<CustomAdapterPlateList.ViewHolder>(), Filterable {

    private val plateFilter: MutableList<Plate>
    private val mFilter: CustomFilterPlate

    init {
        this.plateFilter = ArrayList()
        this.plateFilter.addAll(listPlate)
        this.mFilter = CustomFilterPlate(this@CustomAdapterPlateList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_plate))

    override fun getItemCount() = plateFilter.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(position,
            plateFilter[position], listener)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int, plate: Plate, listener: (Int) -> Unit) = with(itemView) {

            txv_name.text = plate.name
            txv_category.text = plate.category

            setOnClickListener { listener(position) }
        }
    }

    override fun getFilter() = mFilter

    inner class CustomFilterPlate(private val customAdapterPlateList: CustomAdapterPlateList) : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            plateFilter.clear()
            val results = FilterResults()
            if (constraint!!.isEmpty()) {
                plateFilter.addAll(listPlate)
            } else {
                val filterPattern = constraint.toString().toLowerCase(Locale.ROOT).trim()
                for (p in listPlate) {
                    if (p.name.toLowerCase(Locale.ROOT).contains(filterPattern)) {
                        plateFilter.add(p)
                    }
                }
            }
            results.values = plateFilter
            results.count = plateFilter.size
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?)
                = this.customAdapterPlateList.notifyDataSetChanged()
    }
}