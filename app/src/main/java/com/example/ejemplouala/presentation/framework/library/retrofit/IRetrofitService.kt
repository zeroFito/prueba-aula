package com.example.ejemplouala.presentation.framework.library.retrofit

import com.example.ejemplouala.data.dto.MealsResponse
import com.example.ejemplouala.presentation.utils.Constants
import retrofit2.Response
import retrofit2.http.*

interface IRetrofitService {
    @GET(Constants.Apis.ENDPOINT_SEARCH)
    suspend fun getListPlate(@Query("s") s: String): Response<MealsResponse>
}