package com.example.ejemplouala.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.ejemplouala.R
import com.example.ejemplouala.presentation.framework.library.retrofit.Resource
import com.example.ejemplouala.presentation.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun enableProgress(flag: Boolean) = if (flag) {
        clp_progress.visibility = View.VISIBLE
    } else {
        clp_progress.visibility = View.GONE
    }

    inline fun <T> observerResourcesManager(resource: Resource<T>, genericFunction: () -> Unit) {
        when(resource.status) {
            Resource.Status.LOADING -> enableProgress(true)
            Resource.Status.SUCCESS -> {
                enableProgress(false)
                genericFunction()
            }
            Resource.Status.ERROR -> {
                enableProgress(false)
                showToast(resource.message.toString())
            }
        }
    }
}