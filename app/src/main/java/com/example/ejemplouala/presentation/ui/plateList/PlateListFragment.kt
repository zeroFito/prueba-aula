package com.example.ejemplouala.presentation.ui.plateList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.example.ejemplouala.R
import com.example.ejemplouala.presentation.ui.MainActivity
import com.example.ejemplouala.presentation.ui.plateList.adapter.CustomAdapterPlateList
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_plate_list.*

@AndroidEntryPoint
class PlateListFragment : Fragment() {
    private val plateListViewModel: PlateListViewModel by viewModels()
    private lateinit var customAdapterPlateList: CustomAdapterPlateList

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_plate_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // ViewModelProvider(this)[PlateListViewModel::class.java]
        subscribeToViewModel()
        init()
    }

    private fun init() {
        edt_filter.addTextChangedListener { customAdapterPlateList.filter.filter(it.toString()) }
    }

    private fun subscribeToViewModel() {
        plateListViewModel.getPlateLiveData().observe(
            viewLifecycleOwner,
            { resource ->
                (activity as? MainActivity)?.observerResourcesManager(resource) {
                    customAdapterPlateList = CustomAdapterPlateList(resource.data!!) { index ->
                        NavHostFragment.findNavController(this).navigate(
                            PlateListFragmentDirections.actionNavListToDetail(resource.data[index])
                        )
                    }
                    rcv_principal_id.also { recyclerView ->
                        recyclerView.setHasFixedSize(true)
                        recyclerView.swapAdapter(customAdapterPlateList, true)
                    }
                }
            }
        )
    }
}