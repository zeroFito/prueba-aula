package com.example.ejemplouala.presentation.framework.library.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.ejemplouala.data.dto.MealsListResponse
import com.example.ejemplouala.presentation.framework.library.room.converter.DateConverter
import com.example.ejemplouala.presentation.framework.library.room.dao.MealDao
import com.example.ejemplouala.presentation.utils.Constants.Persistence.DATABASE_VERSION

@Database(
    entities = [MealsListResponse::class],
    version = DATABASE_VERSION,
    exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class AppDataBase : RoomDatabase() {
    // --- DAO ---
    abstract fun mealDao(): MealDao
}