package com.example.ejemplouala.presentation.ui.plateDetail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ejemplouala.R
import kotlinx.android.synthetic.main.fragment_plate_detail.*

class PlateDetailFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_plate_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    private fun init() {
        arguments?.let {
            txv_name_detail.text = PlateDetailFragmentArgs.fromBundle(it).plate.name
            txv_instructions_detail.text = PlateDetailFragmentArgs.fromBundle(it).plate.instructions
        }
    }
}