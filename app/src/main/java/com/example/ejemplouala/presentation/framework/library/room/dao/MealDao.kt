package com.example.ejemplouala.presentation.framework.library.room.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.ejemplouala.data.dto.MealsListResponse
import com.example.ejemplouala.presentation.utils.Constants

@Dao
interface MealDao {
    @Insert(onConflict = REPLACE)
    suspend fun saveAll(mealsListResponse: List<MealsListResponse>)

    @Query("SELECT * FROM " + Constants.Persistence.TABLE_NAME)
    fun getMeal(): LiveData<List<MealsListResponse>>
}