package com.example.ejemplouala.presentation.framework.library.retrofit

import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val iRetrofitService: IRetrofitService) : BaseDataSource() {

    suspend fun fetchDataPostListPlate(plate: String) = getResult { iRetrofitService.getListPlate(plate) }
}