package com.example.ejemplouala.presentation.framework.implementation

import com.example.ejemplouala.data.ApiNetwork
import com.example.ejemplouala.data.mapper.PlateMapper
import com.example.ejemplouala.presentation.framework.implementation.strategy.resultLiveData
import com.example.ejemplouala.presentation.framework.library.retrofit.RemoteDataSource
import com.example.ejemplouala.presentation.framework.library.room.dao.MealDao
import javax.inject.Inject

class ImplementationCallRetrofit
@Inject constructor(private val remoteDataSource: RemoteDataSource, private val mealDao: MealDao) : ApiNetwork {
    override fun getListPlate(plate: String) =
        resultLiveData(
            databaseQuery = {
                mealDao.getMeal()
            },
            networkCall = {
                remoteDataSource.fetchDataPostListPlate(plate)
            },
            saveCallResult = {
                mealDao.saveAll(it.meals)
            },
            mapper = {
                PlateMapper().map(it)
            }
        )
}