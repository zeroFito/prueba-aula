package com.example.ejemplouala.presentation.di.modules

import com.example.ejemplouala.MainApplication
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module(includes = [RetrofitModule::class])
@InstallIn(ApplicationComponent::class)
class AppModule {
    @Provides
    @Singleton
    fun provideContext(application: MainApplication) = application
}